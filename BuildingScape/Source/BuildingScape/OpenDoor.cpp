// Copyright information Jorge Garcia 2019


#include "OpenDoor.h"
#include "Engine/World.h"
#include "GameFramework/Actor.h"


// Sets default values for this component's properties
UOpenDoor::UOpenDoor()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UOpenDoor::BeginPlay()
{
	Super::BeginPlay();

	ActorThatOpens = GetWorld()->GetFirstPlayerController()->GetPawn();

	// ...
	
}

void UOpenDoor::OpenDoor() {
	//Get the owner of the component
	AActor* Owner = GetOwner();
	//Create a rotaror
	FRotator NewRotation = FRotator(0.0f, 60.0f, 0.0f);
	//Set new rotation
	Owner->SetActorRotation(NewRotation);
}


// Called every frame
void UOpenDoor::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// Poll the TriggerVolume
	if (PressurePlate->IsOverlappingActor(ActorThatOpens)) {
		// If the ActorThatOpensIs in volume
		OpenDoor();
	}
	else {
	
	}
	
}

